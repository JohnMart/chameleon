package com.chameleon.database;

import java.util.Map;

import com.chameleon.database.exceptions.DatabaseException;
import com.chameleon.utils.Base64Coder;
import com.chameleon.utils.io.PropertiesManager;

public class DatabaseInfo {
    private DbTypeEnum type = null;
    private String host = "";
    private String port = "";
    private String service = "";
    private String username = "";
    private String password = "";
    private String connectionString = "";

    public static enum DbTypeEnum {
        DB2("db2"),
        MARIA("maria"),
        ORACLE("oracle"),
        POSTGRES("postgres"),
        SQLITE("sqlite"),
        SQLSERVER("sqlserver");
        private final String value;

        DbTypeEnum(final String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static DbTypeEnum fromValue(final String v) {
            for (DbTypeEnum c : DbTypeEnum.values()) {
                if (c.value.equalsIgnoreCase(v)) {
                    return c;
                }
            }
            throw new DatabaseException("DB Type entered [ " + v + " ] is not valid. Valid types are [ DB2, MARIA, ORACLE, POSTGRES, SQLITE, SQLSERVER ]");
        }
    }

    public DatabaseInfo(final DbTypeEnum type, final String host, final String port, final String service, final String username, final String password) {
        this.type = type;
        this.host = host;
        this.port = port;
        this.service = service;
        this.username = username;
        this.password = password;
    }

    public DatabaseInfo(final DbTypeEnum type, final String username, final String password, final String connectionString) {
        this.type = type;
        this.connectionString = connectionString;
        this.username = username;
        this.password = password;
    }

    public DatabaseInfo(final String application) {
        final Map<String, String> props = PropertiesManager.properties(String.format("%s/%s.properties", application, application));
        this.type = DbTypeEnum.fromValue(props.get("app.database.type"));
        this.host = props.get("app.database.host");
        this.port = props.get("app.database.port");
        this.service = props.get("app.database.service");
        this.username = props.get("app.database.username");
        this.password = props.get("app.database.password");
    }

    public DatabaseInfo(final String application, final String environment) {
        final Map<String, String> props = PropertiesManager.properties(String.format("config/%s/%s-%s.properties", application, application, environment));
        this.type = DbTypeEnum.fromValue(props.get("app.database.type"));
        this.host = props.get("app.database.host");
        this.port = props.get("app.database.port");
        this.service = props.get("app.database.service");
        this.username = props.get("app.database.username");
        this.password = props.get("app.database.password");
    }

    public DbTypeEnum getType() {
        return type;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return service;
    }

    public String getUser() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConnectionString() {
        return connectionString;
    }

    @Override
    public String toString() {
        String connection = type.value().equals("db2") ? "Database Name- jdbc:AS400://" + service : "Database Connection- jdbc:oracle:thin:@" + host + ":" + port + "/" + service;
        return String.format("Database Type: %s | %s | Database User: %s ", type.value().toUpperCase(), connection, Base64Coder.decodeString(username));
    }
}
