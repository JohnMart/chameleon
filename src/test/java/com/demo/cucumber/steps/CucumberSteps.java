package com.demo.cucumber.steps;

import com.chameleon.utils.TestReporter;
import com.chameleon.utils.cucumber.WebBaseSteps;
import com.demo.cucumber.CucumberDemoPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CucumberSteps extends WebBaseSteps {

    CucumberDemoPage cukeDemoPage;

    @Given("^I have launched the browser$")
    public void i_have_launched_the_browser() {
        cukeDemoPage = new CucumberDemoPage();
        TestReporter.assertFalse(getWebDriver().toString().contains("null"), "Successfully launched the browser.");
    }

    @When("^I navigate to \"([^\"]*)\"$")
    public void i_navigate_to(String website) {
        String url = "";

        switch (website.toLowerCase()) {
            case "etsy":
                url = "https://www.etsy.com";
                break;
            case "google":
                url = "https://www.google.com";
                break;
            case "yahoo":
                url = "https://www.yahoo.com";
                break;
            case "msn":
                url = "https://www.msn.com";
                break;
            default:
                TestReporter.logFailure("The following website alias is not a valid option: [ " + website + " ].");
                break;
        }

        TestReporter.assertTrue(cukeDemoPage.navigateTo(url), "Successfully navigated to [ " + website + " ].");
    }

    @Then("^the \"([^\"]*)\" landing page is displayed$")
    public void the_landing_page_is_displayed(String website) {
        TestReporter.assertTrue(cukeDemoPage.landingPageDisplayed(website), "Successfully verified the "
                + "landing page for [ " + website + " ] is displayed.");
    }

}
