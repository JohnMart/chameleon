package com.chameleon.api.restServices.exceptions;

import com.chameleon.api.WebServiceException;

public class RestException extends WebServiceException {
    private static final long serialVersionUID = -8710980695994382082L;

    public RestException() {
        super("REST Error:");
    }

    public RestException(String message, Object... args) {
        super("REST Error: " + message, args);
    }

    public RestException(String message, Throwable cause, Object... args) {
        super("REST Error: " + message, cause, args);
    }

}
