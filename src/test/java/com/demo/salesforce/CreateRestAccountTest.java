package com.demo.salesforce;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.dataHelpers.personFactory.Person;
import com.demo.salesforce.api.rest.account.AccountProcessor;
import com.demo.salesforce.api.rest.account.request.AccountRequest;
import com.demo.salesforce.api.rest.response.SFResponse;
import com.demo.salesforce.ui.AccountPage;
import com.demo.salesforce.ui.LoginPage;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class CreateRestAccountTest extends WebBaseTest {

    private String sObjectID;
    private Person person;

    @BeforeClass()
    public void setup() {
        setApplicationUnderTest("salesforce");
        setEnvironment(null);
    }

    @Feature("account")
    @Story("Create new rest account")
    @Test(groups = { "demo" })
    public void createRestAccount() {
        TestReporter.logStep("Create an account via Rest");
        person = new Person();

        // Create a new Account object with custom attributes.
        AccountRequest account = new AccountRequest();
        account.setName(person.getFullName());
        account.setDescription("This account was created via REST automation");
        account.setPhone(person.getAllPhones().get(0).getFormattedNumber());

        AccountProcessor accountProcessor = new AccountProcessor();
        SFResponse response = accountProcessor.createAccount(account);
        sObjectID = response.getId();

        TestReporter.assertNotNull(sObjectID, "Verify Account number is returned: " + sObjectID);
        TestReporter.logStep("SOBject Account number: " + sObjectID);

    }

    @Feature("account")
    @Story("Create new account")
    @Test(dependsOnMethods = "createRestAccount", groups = { "demo" })
    public void verifyInUI() {
        setEnvironment(null);
        TestReporter.logStep("Launch salesforce application");
        testStart("verifyInUI");

        TestReporter.logStep("Login to salesforce");
        LoginPage loginPage = new LoginPage();
        loginPage.validLogin();

        TestReporter.logStep("Navigate to the Account record created in Salesforce via SOAP API");
        DriverManager.getDriver().get("https://na59.salesforce.com/" + sObjectID);
        AccountPage accountPage = new AccountPage();

        TestReporter.logStep("Verify account is displayed successfully for: " + person.getFullName());
        TestReporter.assertTrue(accountPage.verifyAccountPageIsDisplayed(person.getFullName()),
                "Verify the account page displayed successfully");

    }

    @AfterClass()
    public void deleteSOBject() {
        DriverManager.quitDriver();
        AccountProcessor accountProcessor = new AccountProcessor();
        accountProcessor.deleteAccount(sObjectID);
    }

}
