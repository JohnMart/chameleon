package com.chameleon.selenium.web;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestAlertHandler extends WebBaseTest {
    ExtendedWebDriver driver = null;

    @BeforeClass(groups = { "regression", "utils", "dev", "framehandler" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/utils/alertHandler.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Utilities")
    @Story("AlertHandler")

    @Test(groups = { "regression", "interfaces", "textbox" })
    public void constructorWithElement() {
        driver = testStart("TestAlert");
        Assert.assertNotNull((new AlertHandler()));
    }

    @Feature("Utilities")
    @Story("AlertHandler")

    @Test(groups = { "regression", "utils", "dev", "alertHandler" }, dependsOnMethods = "constructorWithElement")
    public void isAlertPresent() {
        Assert.assertTrue(AlertHandler.isAlertPresent(driver, 1), "Alert was not present");
    }

    @Feature("Utilities")
    @Story("AlertHandler")

    @Test(groups = { "regression", "utils", "dev", "alertHandler" }, dependsOnMethods = "isAlertPresent")
    public void handleAllAlerts() {
        AlertHandler.handleAllAlerts(driver, 1);
        Assert.assertFalse(AlertHandler.isAlertPresent(driver, 1), "Alert was not handled");
    }

    @Feature("Utilities")
    @Story("AlertHandler")

    @Test(groups = { "regression", "utils", "dev", "alertHandler" }, dependsOnMethods = "handleAllAlerts")
    public void handleAlert() {
        driver.findElement(By.id("button")).click();
        AlertHandler.handleAlert(driver, 1);
        Assert.assertFalse(AlertHandler.isAlertPresent(driver, 1), "Alert was not handled");
    }

}
