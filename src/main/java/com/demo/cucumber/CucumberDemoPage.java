package com.demo.cucumber;

import org.openqa.selenium.By;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebPageLoaded;
import com.chameleon.utils.TestReporter;

public class CucumberDemoPage {
	
	private ExtendedWebDriver driver;
	
	By etsyLogo = By.xpath("//span[@class='etsy-icon etsy-logo-svg-full']");
	By googleLogo = By.xpath("//img[contains(@alt, 'Google')]");
	By yahooLogo = By.xpath("//a[@id='uh-logo']");
	By msnLogo = By.xpath("//a[@class='logo' and contains(text(), 'msn')]");
	
	public CucumberDemoPage() {
		driver = DriverManager.getWebDriver();
	}
	
	/**
	 * Navigates to a specified website.
	 * 
	 * @author John Martin
	 * @date 07/29/2018
	 * @param website - value of the website to navigate to
	 * @return boolean - successful navigation to specified website
	 */
	public boolean navigateTo(String website) {
		driver.get(website);
		WebPageLoaded.isDomComplete();
		return driver.getCurrentUrl().contains(website);
	}
	
	/**
	 * Verifies the specified website's landing page is displayed.
	 * 
	 * @author John Martin
	 * @date 07/29/2018
	 * @param website - value of the website to verify landing page
	 * @return boolean - successful display of the specified website's landing page
	 */
	public boolean landingPageDisplayed(String website) {
		switch (website.toLowerCase()) {
		case "etsy":
			return driver.findElement(etsyLogo).syncVisible();
		case "google":
			return driver.findElement(googleLogo).syncVisible();
		case "yahoo":
			return driver.findElement(yahooLogo).syncVisible();
		case "msn":
			return driver.findElement(msnLogo).syncVisible();
		default:
			TestReporter.logFailure("The following website value is not a valid option: [ "+ website +" ].");
			return false;
		}
	}

}
