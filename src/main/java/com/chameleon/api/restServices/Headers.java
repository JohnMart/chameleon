package com.chameleon.api.restServices;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;

import com.chameleon.utils.Randomness;

public class Headers {

    @SuppressWarnings("serial")
    public static Map<String, String> AUTH() {
        return new HashMap<String, String>() {
            {
                put(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
            }
        };
    }

    @SuppressWarnings("serial")
    public static Map<String, String> BASIC_CONVO() {
        return new HashMap<String, String>() {
            {
                put(HttpHeaders.CONTENT_TYPE, "application/json;charset=utf-8");
                put(HttpHeaders.ACCEPT, "application/json");
                put(HttpHeaders.CONNECTION, "keep-alive");
                put("x-corrolation-id", Randomness.generateMessageId());
                put("x-username", "test.user");
                put("x-requested-timestamp", Randomness.generateCurrentXMLDatetime());
            }
        };
    }

    @SuppressWarnings("serial")
    public static Map<String, String> JSON() {
        return new HashMap<String, String>() {
            {
                put(HttpHeaders.CONTENT_TYPE, "application/json");
            }
        };
    }

    @SuppressWarnings("serial")
    public static Map<String, String> XML() {
        return new HashMap<String, String>() {
            {
                put(HttpHeaders.CONTENT_TYPE, "application/xml");
            }
        };
    }

}