package com.chameleon.utils.cucumber;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CucumberResults {
    private int totalFeatureCount = 0;
    private int totalScenarioCount = 0;
    private int totalStepCount = 0;

    private int featurePassCount = 0;
    private int scenarioPassCount = 0;
    private int stepPassCount = 0;

    private int featureSkipCount = 0;
    private int scenarioSkipCount = 0;
    private int stepSkipCount = 0;

    private int featureFailCount = 0;
    private int scenarioFailCount = 0;
    private int stepFailCount = 0;

    private Map<String, Integer> exceptions = new HashMap<>();
    private Map<String, Long> stepExecution = new HashMap<>();

    public int getTotalFeatureCount() {
        return totalFeatureCount;
    }

    public int getTotalScenarioCount() {
        return totalScenarioCount;
    }

    public int getTotalStepCount() {
        return totalStepCount;
    }

    public int getFeaturePassCount() {
        return featurePassCount;
    }

    public int getScenarioPassCount() {
        return scenarioPassCount;
    }

    public int getStepPassCount() {
        return stepPassCount;
    }

    public int getFeatureSkipCount() {
        return featureSkipCount;
    }

    public int getScenarioSkipCount() {
        return scenarioSkipCount;
    }

    public int getStepSkipCount() {
        return stepSkipCount;
    }

    public int getFeatureFailCount() {
        return featureFailCount;
    }

    public int getScenarioFailCount() {
        return scenarioFailCount;
    }

    public int getStepFailCount() {
        return stepFailCount;
    }

    public void incrementTotalFeatureCount() {
        totalFeatureCount++;
    }

    public void incrementTotalScenarioCount() {
        totalScenarioCount++;
    }

    public void incrementTotalStepCount() {
        totalStepCount++;
    }

    public void incrementFeaturePassCount() {
        featurePassCount++;
    }

    public void incrementScenarioPassCount() {
        scenarioPassCount++;
    }

    public void incrementStepPassCount() {
        stepPassCount++;
    }

    public void incrementFeatureSkipCount() {
        featureSkipCount++;
    }

    public void incrementScenarioSkipCount() {
        scenarioSkipCount++;
    }

    public void incrementStepSkipCount() {
        stepSkipCount++;
    }

    public void incrementFeatureFailCount() {
        featureFailCount++;
    }

    public void incrementScenarioFailCount() {
        scenarioFailCount++;
    }

    public void incrementStepFailCount() {
        stepFailCount++;
    }

    public String getPassRate() {
        final float rate = (Float.valueOf(scenarioPassCount) / Float.valueOf(totalScenarioCount)) * 100;
        final DecimalFormat df = new DecimalFormat("###.##");
        return df.format(rate);
    }

    public String addException(final String exception) {

        String trimmedException = exception.substring(0, exception.indexOf("\n\tat"));

        if (trimmedException.contains("Build info:")) {
            trimmedException = trimmedException.substring(0, exception.indexOf("Build info:"));
        }

        trimmedException = trimmedException.replaceAll("\\[ \\d*.\\d* \\] seconds", "DEFAULT timeout");

        if (exceptions.containsKey(trimmedException)) {
            final int count = exceptions.get(trimmedException);
            exceptions.put(trimmedException, count + 1);
        } else {
            exceptions.put(trimmedException, 1);
        }

        return trimmedException;
    }

    public Map<String, Integer> getExceptions() {

        // sort algorithm to sort most common exceptions to least common
        return exceptions.entrySet().stream()
                .sorted(Map.Entry.<String, Integer> comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

    }

    public Map<String, Integer> getExceptions(final int numberToReturn) {

        // sort algorithm to sort most common exceptions to least common
        return exceptions.entrySet().stream()
                .sorted(Map.Entry.<String, Integer> comparingByValue().reversed())
                .limit(numberToReturn)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

    }

    public void addStepDuration(final String stepName, final Long duration) {
        if (!stepExecution.containsKey(stepName) || stepExecution.get(stepName) < duration) {
            stepExecution.put(stepName, duration);
        }
    }

    public Map<String, Long> getStepDurations() {

        // sort algorithm to sort most common exceptions to least common
        return stepExecution.entrySet().stream()
                .sorted(Map.Entry.<String, Long> comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

    }

    public Map<String, Long> getStepDurations(final int numberToReturn) {

        // sort algorithm to sort most common exceptions to least common
        return stepExecution.entrySet().stream()
                .sorted(Map.Entry.<String, Long> comparingByValue().reversed())
                .limit(numberToReturn)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

    }
}
