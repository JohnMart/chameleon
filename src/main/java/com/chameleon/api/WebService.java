package com.chameleon.api;

public interface WebService {
    public String getRequest();

    public String getResponse();

    public String getUrl();

    public String getServiceName();

    public String getOperationName();

    public String getExecutionTime();

    public String getWebServiceType();
}
