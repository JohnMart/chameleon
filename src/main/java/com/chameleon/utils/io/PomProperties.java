package com.chameleon.utils.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

public class PomProperties {

    public static String getProperty(String property) {

        String propertyValue = System.getProperty(property);

        if (propertyValue != null && !propertyValue.isEmpty()) {
            System.out.println("Env Prop: " + property + "-" + propertyValue.trim());
            return propertyValue.trim();
        }

        final File pomfile = new File("pom.xml");

        try (FileReader reader = new FileReader(pomfile)) {
            MavenXpp3Reader mavenreader = new MavenXpp3Reader();
            Model model = mavenreader.read(reader);
            MavenProject project = new MavenProject(model);
            project.setFile(pomfile);
            propertyValue = project.getProperties().getProperty(property);
            if (propertyValue != null && !propertyValue.isEmpty()) {
                System.out.println("Pom Prop: " + property + "-" + propertyValue.trim());
                return propertyValue.trim();
            }
        } catch (IOException | XmlPullParserException e) {
            return null;
        }
        return null;
    }

}