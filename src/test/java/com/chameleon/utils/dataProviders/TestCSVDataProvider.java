package com.chameleon.utils.dataProviders;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.utils.dataProviders.CSVDataProvider;
import com.chameleon.utils.exception.DataProviderInputFileNotFound;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestCSVDataProvider {
    @Feature("Utilities")
    @Story("CSVDataProvider")
    
    @Test(groups = { "regression", "utils", "dataProviders" })
    public void getData() {
        Object[][] data = CSVDataProvider.getData("/excelsheets/TestData_csv.csv");
        Assert.assertNotNull(data);
    }

    @Feature("Utilities")
    @Story("CSVDataProvider")
    
    @Test(groups = { "regression", "utils", "dataProviders" })
    public void getDataWithDelimiter() {
        Object[][] data = CSVDataProvider.getData("/excelsheets/TestData_csv.csv", ",");
        Assert.assertNotNull(data);
    }

    @Feature("Utilities")
    @Story("CSVDataProvider")
    
    @Test(groups = { "regression", "utils", "dataProviders" }, expectedExceptions = DataProviderInputFileNotFound.class)
    public void getDataNoFileFound() {
        Object[][] data = CSVDataProvider.getData("blah");
        Assert.assertNotNull(data);
    }
}
