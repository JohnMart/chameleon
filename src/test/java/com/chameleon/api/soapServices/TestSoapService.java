package com.chameleon.api.soapServices;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.api.APIBaseTest;
import com.chameleon.api.soapServices.exceptions.HeaderNotFoundException;
import com.chameleon.api.soapServices.exceptions.MissingFunctionParameterValueException;
import com.chameleon.api.soapServices.exceptions.SoapException;
import com.chameleon.api.soapServices.helpers.GetActorsById;
import com.chameleon.api.soapServices.helpers.SoapTraining;
import com.chameleon.utils.exception.XPathInvalidExpression;
import com.chameleon.utils.exception.XPathNotFoundException;
import com.chameleon.utils.exception.XPathNullNodeValueException;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestSoapService extends APIBaseTest {
    @Feature("API")
    @Story("SoapServices")

    @Test
    public void createService() {
        SoapTraining usZip = new SoapTraining();
        Assert.assertNotNull(usZip);
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createService")
    public void createOperation() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertNotNull(getInfo);
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void createOperation_WithCSVData() {
        GetActorsById getInfo = new GetActorsById("Main", "CSV");
        Assert.assertTrue(getInfo.getRequestActorId().equals("123"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void createOperation_WithXLSData() {
        GetActorsById getInfo = new GetActorsById("Main", "XLS");
        Assert.assertTrue(getInfo.getRequestActorId().equals("123"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void createOperation_WithXLSXData() {
        GetActorsById getInfo = new GetActorsById("Main", "XLSX");
        Assert.assertTrue(getInfo.getRequestActorId().equals("123"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void addRequestHeader() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.addRequestHeader("blah", "blah");
        Assert.assertNotNull(getInfo);
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void getNumberOfRequestNodesByXPath() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertTrue(getInfo.getNumberOfRequestNodesByXPath("/Envelope/Body/getActorsByIdRequest/actor_id") == 1);
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setSoapVersion() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setSoapVersion();
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void getServiceURL() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertTrue(getInfo.getUrl().equals("https://training-server.herokuapp.com:443/soap"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void getServiceName() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertTrue(getInfo.getServiceName().equals("SoapTraining"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void getOperationName() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertTrue(getInfo.getOperationName().equals("getActorsById"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void getRequest() {
        GetActorsById getInfo = new GetActorsById();
        Assert.assertTrue(!getInfo.getRequest().isEmpty());
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddAttribute() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addattribute;blah");
        Assert.assertTrue(getInfo.getRequest().contains("<my:actor_id blah=\"\""));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddAttribute_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addattribute");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNode() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest", "fx:addnode;blah");
        Assert.assertTrue(getInfo.getRequest().contains("<blah/>"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNode_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addnode");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNodes() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest", "fx:addnodes;blah/blah2/blah3");
        Assert.assertTrue(getInfo.getRequest().replace(System.getProperty("line.separator"), "").replaceAll(" ", "").contains("<blah><blah2><blah3/></blah2></blah>"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNodes_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addnodes");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNamespace() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope", "fx:addnamespace;xmlns:web2,http://www.webserviceXnew.NET");
        Assert.assertTrue(getInfo.getRequest().contains("xmlns:web2=\"http://www.webserviceXnew.NET\""));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNamespace_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addnamespace");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_AddNamespace_MissingURLParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:addnamespace;xmlns:web2");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_GetDateTime() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:getdatetime;0");
        Assert.assertTrue(getInfo.getRequestActorId().matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_GetDateTime_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:getdatetime");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_GetDate() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:getdate;0");
        Assert.assertTrue(getInfo.getRequestActorId().matches("[0-9]{4}-[0-9]{2}-[0-9]{2}"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_GetDate_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:getdate");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_RemoveAttribute() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("//Envelope/Body/getActorsByIdRequest/actor_id", "fx:addAttribute;blah");
        getInfo.setRequestNodeValueByXPath("//Envelope/Body/getActorsByIdRequest/actor_id/@blah", "fx:removeattribute");
        Assert.assertTrue(getInfo.getRequest().contains("<my:actor_id>123</my:actor_id>"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_RemoveNode() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("//Envelope/Body/getActorsByIdRequest/actor_id", "fx:removenode");
        Assert.assertTrue(getInfo.getRequest().contains("<my:getActorsByIdRequest/>"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomAlphaNumeric() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:randomalphanumeric;2");
        Assert.assertTrue(getInfo.getRequestActorId().matches("[0-9 a-z A-Z]{2}"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomAlphaNumeric_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:randomalphanumeric");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomNumber() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:randomnumber;2");
        Assert.assertTrue(getInfo.getRequestActorId().matches("[0-9]{2}"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomNumber_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:RandomNumber");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomString() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:RandomString;5");
        Assert.assertTrue(getInfo.getRequestActorId().matches("[a-z A-Z]{5}"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = SoapException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_InvalidCommand() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:blah");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = MissingFunctionParameterValueException.class)
    public void setRequestNodeValueByXPath_HandleValueFunctions_RandomString_MissingParameter() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "fx:RandomString");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = XPathNotFoundException.class)
    public void setRequestNodeValueByXPath_XPathNotFoundException() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/blah", "blah");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = XPathInvalidExpression.class)
    public void setRequestNodeValueByXPath_InvalidXPathExpression() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/blah\"", "blah");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation", expectedExceptions = XPathNullNodeValueException.class)
    public void setRequestNodeValueByXPath_NullValueExpression() {
        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body/getActorsByIdRequest/actor_id", "");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void sendRequest() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void sendRequestExpectFault() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.setRequestNodeValueByXPath("/Envelope/Body", SoapServiceCommands.removeNode());
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.getResponseStatusCode().equals("SOAP-ENV:Server"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "createOperation")
    public void sendRequestWithHeaders() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.addRequestHeader("encoding", "UTF-8");
        getInfo.sendRequest();
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void getResponse() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(!getInfo.getResponse().isEmpty());
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void getNumberOfResponseNodesByXPath() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.getNumberOfResponseNodesByXPath("/Envelope/Body/getActorsByIdResponse/actor") == 1);
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void getResponseNodeValueByXPath() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.getResponseNodeValueByXPath("/Envelope/Body/getActorsByIdResponse/actor/actor_id").equals("123"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void getResponseHeaders() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.getResponseHeader("Content-Type").equals("text/xml; charset=utf-8"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest", expectedExceptions = HeaderNotFoundException.class)
    public void getResponseHeaders_NoneFound() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        getInfo.getResponseHeader("blah");
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void validateResponse_WithCSVData() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.validateResponse("/excelsheets/GetActorsByIdResponse_csv.csv", "Main"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void validateResponse_WithXLSData() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.validateResponse("/excelsheets/GetActorsByIdResponse_xls.xls", "Main"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void validateResponse_WithXLSXData() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertTrue(getInfo.validateResponse("/excelsheets/GetActorsByIdResponse_xlsx.xlsx", "Main"));
    }

    @Feature("API")
    @Story("SoapServices")

    @Test(dependsOnMethods = "sendRequest")
    public void validateResponse_WithErrors() {

        GetActorsById getInfo = new GetActorsById();
        getInfo.sendRequest();
        Assert.assertFalse(getInfo.validateResponse("/excelsheets/GetActorsByIdResponse_ExpectErrors.csv", "Main"));
    }

}
