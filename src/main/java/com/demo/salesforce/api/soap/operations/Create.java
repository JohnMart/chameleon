package com.demo.salesforce.api.soap.operations;

import java.io.File;

import com.chameleon.utils.TestReporter;
import com.chameleon.utils.XMLTools;
import com.chameleon.utils.dataHelpers.personFactory.Person;
import com.chameleon.utils.exception.XPathNotFoundException;
import com.demo.salesforce.api.soap.SalesforceSoapService;

public class Create extends SalesforceSoapService {

    public Create() {
        File xml = new File(this.getClass().getResource("/xmls/salesforce/CreateRequest.xml").getPath());
        setOperationName("Create Salesforce SObject");
        setRequestDocument(XMLTools.makeXMLDocument(xml));
        removeComments();
        removeWhiteSpace();
        verifyAuthentication();
        setSessionID(sessionID);
        addRequestHeader("SoapAction", "Test");
        setServiceURL(getUrl().replace("/c/", "/u/"));
    }

    /**
     * Creates a client account in the salesforce api and returns the new account ID
     *
     * @param accName
     * @param city
     * @param state
     * @param country
     * @param streetAddress
     * @return account ID
     */
    public String createClientAccount(String accName, String city, String state, String country, String streetAddress, String zipcode) {

        setSObjectNodeByNameAndValue("Name", accName);
        // setSObjectNodeByNameAndValue("RecordTypeId", SalesforceStaticAPIData.COMPETITOR_ACCOUNT_RECORDTYPEID);
        setSObjectNodeByNameAndValue("BillingCity", city);
        setSObjectNodeByNameAndValue("BillingState", state);
        setSObjectNodeByNameAndValue("BillingCountry", country);
        setSObjectNodeByNameAndValue("BillingStreet", streetAddress);
        setSObjectNodeByNameAndValue("BillingPostalCode", zipcode);
        // Send the soap create request
        sendRequest();
        // Log soap response results to the reporter
        TestReporter.logAPI(getResponseStatusCode().equals("200"), "Details regarding the create request", this);
        // Verify a 200 response comes back
        TestReporter.assertTrue(getResponseStatusCode().equals("200"), "Verify the create request was successful");
        // Verify no errors were returned
        TestReporter.assertTrue(isSuccess(), "Create Account request not successful - error message: " + getErrorMessage());
        // return the sobject ID
        return getID();

    }

    /**
     * Creates a client account in the salesforce api and returns the new account ID
     *
     * @param person
     * @return account ID
     */
    public String createClientAccount(final Person person) {
        setSObjectNodeByNameAndValue("Name", person.getFullName());
        // setSObjectNodeByNameAndValue("RecordTypeId", SalesforceStaticAPIData.COMPETITOR_ACCOUNT_RECORDTYPEID);
        setSObjectNodeByNameAndValue("BillingCity", person.primaryAddress().getCity());
        setSObjectNodeByNameAndValue("BillingState", person.primaryAddress().getStateAbbv());
        setSObjectNodeByNameAndValue("BillingCountry", person.primaryAddress().getCountryAbbv());
        setSObjectNodeByNameAndValue("BillingStreet", person.primaryAddress().getStreetName());
        setSObjectNodeByNameAndValue("BillingPostalCode", person.primaryAddress().getZipCode());
        // Send the soap create request
        sendRequest();
        // Log soap response results to the reporter
        TestReporter.logAPI(getResponseStatusCode().equals("200"), "Details regarding the create request", this);
        // Verify a 200 response comes back
        TestReporter.assertTrue(getResponseStatusCode().equals("200"), "Verify the create request was successful");
        // Verify no errors were returned
        TestReporter.assertTrue(isSuccess(), "Create Account request not successful");
        // return the sobject ID
        return getID();

    }

    // ------------Getters & Setters: Create Request & Response------------

    public void setSessionID(String value) {
        setRequestNodeValueByXPath("/Envelope/Header/SessionHeader/sessionId", value);
    }

    public void setSObjectNodeByNameAndValue(String nodeName, String value) {
        setRequestNodeValueByXPath("/Envelope/Body/create/sObjects/" + nodeName, value);
    }

    public String getID() {
        return getResponseNodeValueByXPath("/Envelope/Body/createResponse/result/id");
    }

    public String getSuccess() {
        return getResponseNodeValueByXPath("/Envelope/Body/createResponse/result/success");
    }

    public boolean isSuccess() {
        try {
            return getSuccess().equals("true");
        } catch (XPathNotFoundException e) {
            return false;
        }
    }

    public String getErrorMessage() {
        return getResponseNodeValueByXPath("/Envelope/Body/createResponse/result/errors/message");
    }
}
