package com.chameleon.utils;

import org.apache.commons.lang3.time.StopWatch;

import com.chameleon.AutomationException;
import com.chameleon.ChameleonGlobalProperties;

public class Sleeper {

    public static void sleep(double seconds) {
        final double maxSleepTime = ChameleonGlobalProperties.getDefaultMaxSleep();

        if (seconds >= maxSleepTime) {
            throw new AutomationException("Sleep time exceeds " + maxSleepTime + " seconds.");
        }
        TestReporter.logTrace("Sleeping for [ " + seconds + " ] seconds");
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        do {
        } while ((stopwatch.getTime() / 1000.0) < seconds);
        stopwatch.stop();
        stopwatch.reset();
    }
}
