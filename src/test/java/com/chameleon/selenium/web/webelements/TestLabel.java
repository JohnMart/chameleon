package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Label;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebLabel;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestLabel extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "label", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/label.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Label")

    @Test(groups = { "regression", "interfaces", "label" })
    public void constructorWithElement() {
        driver = testStart("TestLabel");
        Assert.assertNotNull((new WebLabel(driver, (By.xpath("//*[@id='radioForm']/label[1]")))));
    }

    @Feature("Element Interfaces")
    @Story("Label")

    @Test(groups = { "regression", "interfaces", "label" }, dependsOnMethods = "constructorWithElement")
    public void getFor() {
        Label label = driver.findLabel(By.xpath("//*[@id='radioForm']/label[1]"));
        Assert.assertTrue(label.getFor().equals("genderm"));
    }

}
