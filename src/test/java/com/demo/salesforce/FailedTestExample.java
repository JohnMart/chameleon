package com.demo.salesforce;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.dataHelpers.personFactory.Person;
import com.demo.salesforce.api.soap.operations.Create;
import com.demo.salesforce.api.soap.operations.Delete;
import com.demo.salesforce.ui.AccountPage;
import com.demo.salesforce.ui.LoginPage;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class FailedTestExample extends WebBaseTest {

    private String sObjectID;
    private Person person;

    @BeforeClass(groups = { "regression", "interfaces", "button", "dev" })
    public void setup() {
        setApplicationUnderTest("salesforce");
        setEnvironment(null);
    }

    @Feature("account")
    @Story("Create new account")
    @Test(groups = { "demo" })
    public void createSFAccount() {
        TestReporter.logStep("Create an account via SOAP");
        person = new Person();
        Create create = new Create();
        sObjectID = create.createClientAccount(person);
        TestReporter.assertNotNull(sObjectID, "Verify Account number is returned: " + sObjectID);
        TestReporter.logStep("SOBject Account number: " + sObjectID);

    }

    @Feature("account")
    @Story("Failed test example")
    @Test(groups = { "demo" })
    public void failedTestExample() {
        TestReporter.logStep("Launch salesforce application");
        testStart("verifyInUI");

        TestReporter.logStep("Login to salesforce");
        LoginPage loginPage = new LoginPage();
        loginPage.validLogin();

        TestReporter.logStep("Navigate to the Account record created in Salesforce via SOAP API");
        DriverManager.getDriver().get("https://na59.salesforce.com/" + sObjectID);
        AccountPage accountPage = new AccountPage();

        TestReporter.logStep("Verify account is displayed successfully for: FailedTestName");
        TestReporter.assertTrue(accountPage.verifyAccountPageIsDisplayed("FailedTestName"),
                "Verify the account page displayed successfully");
    }

    @AfterClass()
    public void deleteSOBject() {
        DriverManager.quitDriver();
        Delete delete = new Delete();
        delete.deleteObject(sObjectID);
    }

}
