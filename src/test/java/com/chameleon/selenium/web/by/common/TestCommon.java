package com.chameleon.selenium.web.by.common;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebElement;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestCommon extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "common", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/element.html");
        setEnvironment(null);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("ByCommon")

    @Test(groups = { "regression", "interfaces" })
    public void driverFindTextValue() {
        driver = testStart("TestCommon");
        WebElement element = driver.findElement(ByCommon.textValue("Element test page"));
        Assert.assertTrue(element.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindTextValue", expectedExceptions = IllegalArgumentException.class)
    public void driverFindTextValueNullSearchBy() {
        driver.findElement(ByCommon.textValue(""));
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindTextValue")
    public void driverFindTextValueContains() {
        WebElement element = driver.findElement(ByCommon.textValueContains("Element"));
        Assert.assertTrue(element.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindTextValue", expectedExceptions = IllegalArgumentException.class)
    public void driverFindTextValueContainsNullSearchBy() {
        driver.findElement(ByCommon.textValueContains(""));
    }

}
