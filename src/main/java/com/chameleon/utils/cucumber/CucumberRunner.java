package com.chameleon.utils.cucumber;

import org.apache.commons.lang.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.chameleon.utils.TestReporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {"src/test/java/com/demo/cucumber/features"}, glue = {"com.demo.cucumber.steps"}, tags = "@cuke")
public class CucumberRunner extends AbstractTestNGCucumberTests {
	
	/**
	 * BeforeSuite TestNG annotation to allow the 'environment' property to be set
	 * from the TestNG 'runner.xml' file; this allows Jenkins execution and local TestNG
	 * execution to be independent from one another and allows the end user to change their
	 * target environment via a parameter in the TestNG 'runner.xml'
	 * 
	 * This can be expanded to receive and set other TestNG runner.xml level parameters.
	 * 
	 * @author John Martin
	 * @date 07/04/2018
	 * @param environment - this value comes from the TestNG XML runner file.
	 * @throws RuntimeException
	 */
	@BeforeMethod(alwaysRun = true)
	@Parameters({ "environment", "browser", "loglevel" })
	public void setUpLocalExecution(String environment, String browser, @Optional String logLevel) {
		//Setting the test.envrionment system property.
		//This will overwrite the existing value given it is coming from
		//the TestNG launching it; this parameter is used to determine which
		//user to choose and ultimately which environment is launched.
		System.setProperty("test.environment", environment);
		System.setProperty("browser", browser);
		
		//Setting the log level for the Chameleon framework via the parameter
		//provided via the runner.xml
		if (StringUtils.isNotEmpty(logLevel)) {
			TestReporter.log("Setting Chameleon Test Reporter log level to [ " + logLevel + " ]");
            TestReporter.setDebugLevel(determineLogLevel(logLevel));
		}
	}
	
	/**
	 * Used to determine the appropriate logging level to set.
	 * Copy of existing method in BaseTest in Chameleon.
	 * 
	 * @author John Martin
	 * @date 07/04/2018
	 * @param level
	 * @return int - the numeric debug level: 1, 2, or 3.
	 */
	private int determineLogLevel(String level) {
        switch (level.toUpperCase()) {
            case "1":
            case "INFO":
                return 1;

            case "2":
            case "DEBUG":
                return 2;

            case "3":
            case "TRACE":
                return 3;

            default:
                return 0;
        }
    }

}