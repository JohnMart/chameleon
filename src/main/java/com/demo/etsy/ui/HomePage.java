package com.demo.etsy.ui;

import org.openqa.selenium.By;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;

public class HomePage {

    private ExtendedWebDriver driver;

    // By locators for elements in the DOM.
    By icoEtsy = By.id("etsy-logo");

    // Page Object constructor.
    public HomePage() {
        driver = DriverManager.getWebDriver();
    }

    // Verify the Etsy home page is displayed.
    public boolean verifyHomepageIsDisplayed() {
        return driver.findElement(icoEtsy).syncVisible(10);
    }

}
