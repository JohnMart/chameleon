package com.chameleon.database.databaseImpl;

import com.chameleon.database.Database;
import com.chameleon.database.DatabaseInfo;

/**
 * @deprecated
 *             Current process can lead to unnecessary configuration for the user.
 *             Instead use the {@link DatabaseInfo}
 * @author Justin Phlegar
 *
 */
@Deprecated
public class SQLiteDatabase extends Database {
    public SQLiteDatabase(String filepath) {
        super.driver = null;
        super.connectionString = "jdbc:sqlite:" + filepath;
        super.isTypeForwardOnly = true;
    }
}
