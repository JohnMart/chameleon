package com.chameleon.api.restServices;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.api.APIBaseTest;
import com.chameleon.api.restServices.exceptions.RestException;
import com.chameleon.api.restServices.helpers.PostRequest;
import com.chameleon.api.restServices.helpers.PostsResponse;
import com.chameleon.utils.io.JsonObjectMapper;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestRestResponse extends APIBaseTest {
    public final static String basePostsUrl = "https://jsonplaceholder.typicode.com/posts";
    private RestResponse response;

    @Feature("API")
    @Story("RestServices")
    @Test
    public void createRestResponse() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertNotNull(response);
    }

    @Feature("API")
    @Story("RestServices")
    @Test
    public void getStatusCode() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getResponse() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(!response.getResponse().isEmpty());
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getResponseAsXMLNode() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(!response.getResponseAsXML().isEmpty());
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getResponseAsXMLArray() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl);
        Assert.assertTrue(!response.getResponseAsXML().isEmpty());
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getResponseFormat() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getResponseFormat().equals("json"));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getMethod() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getMethod().equals("GET"));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getRequest() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertNotNull(response.getRequest());
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getRequestBody() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");
        RestService rest = new RestService();
        response = rest.sendPostRequest(basePostsUrl + "/1", Headers.JSON(), JsonObjectMapper.getJsonFromObject(request));
        Assert.assertTrue(StringUtils.isNotEmpty(response.getRequest()));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getURL() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getUrl().equals("https://jsonplaceholder.typicode.com/posts/1"));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getHeaders() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getHeaders().length > 0);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getHeadersNotFound() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertNull(response.getHeader("blah"));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void getHeader() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        Assert.assertTrue(response.getHeader("Content-Type").equals("application/json; charset=utf-8"));
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void mapJSONToObject() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        PostsResponse post = response.mapJSONToObject(PostsResponse.class);
        Assert.assertNotNull(post);
    }

    @Feature("API")
    @Story("RestServices")

    @Test(expectedExceptions = RestException.class)
    public void mapJSONToObjectInvalidJson() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        response.mapJSONToObject("{{blah:1}", PostsResponse.class);
    }

    @Feature("API")
    @Story("RestServices")

    @Test(expectedExceptions = RestException.class)
    public void mapJSONToObjectIncorrectJson() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        response.mapJSONToObject("{\"userId\":1,\"id\":1,\"blah\":1}", PostsResponse.class);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void mapJSONToTree() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        response.mapJSONToTree();
    }

    @Feature("API")
    @Story("RestServices")

    @Test(expectedExceptions = RestException.class)
    public void mapJSONToTreeInvalidJson() {
        RestService rest = new RestService();
        response = rest.sendGetRequest(basePostsUrl + "/1");
        response.mapJSONToTree("{{blah:1}");
    }
}
