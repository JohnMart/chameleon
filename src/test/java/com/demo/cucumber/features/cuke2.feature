@cucumberdemo @cuke
Feature: Chameleon Demo Cuke

  Background: Launch the browser
    Given I have launched the browser

  Scenario: Navigate to Yahoo
    When I navigate to "Yahoo"
    Then the "Yahoo" landing page is displayed
  
  Scenario: Navigate to MSN
    When I navigate to "MSN"
    Then the "MSN" landing page is displayed