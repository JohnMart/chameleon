package com.chameleon;

public class AutomationException extends RuntimeException {

    private static final long serialVersionUID = -8710980695994382082L;

    public AutomationException() {
        super("Automation Error");
    }

    public AutomationException(String message, Object... args) {
        super("Automation Error: " + String.format(message, args));
    }

    public AutomationException(String message, Throwable cause, Object... args) {
        super("Automation Error: " + String.format(message, args), cause);
    }

}
