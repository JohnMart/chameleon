package com.chameleon.database.databaseImpl;

import com.chameleon.database.Database;
import com.chameleon.database.DatabaseInfo;
import com.chameleon.utils.Constants;

/**
 * @deprecated
 *             Current process can lead to unnecessary configuration for the user.
 *             Instead use the {@link DatabaseInfo}
 * @author Justin Phlegar
 *
 */
@Deprecated
public class OracleDatabase extends Database {

    public OracleDatabase(String tnsName) {
        String tns = getClass()
                .getResource(Constants.TNSNAMES_PATH + "tnsnames.ora")
                .getPath().toString();
        tns = tns.substring(0, tns.lastIndexOf("/"));
        System.setProperty("oracle.net.tns_admin", tns);

        super.driver = "oracle.jdbc.driver.OracleDriver";
        super.connectionString = "jdbc:oracle:thin:@" + tnsName.toUpperCase();
    }

    public OracleDatabase(String host, String port, String sid) {
        super.driver = "oracle.jdbc.driver.OracleDriver";
        super.connectionString = "jdbc:oracle:thin:@" + host + ":" + port + "/" + sid;
    }
}
