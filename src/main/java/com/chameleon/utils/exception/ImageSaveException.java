package com.chameleon.utils.exception;

import com.chameleon.AutomationException;

public class ImageSaveException extends AutomationException {
    private static final long serialVersionUID = 3407361723082329697L;

    public ImageSaveException(String message) {
        super(message);
    }

    public ImageSaveException(String message, Throwable cause) {
        super(message, cause);
    }

}
