package com.chameleon.utils.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Match {

    @JsonProperty("arguments")
    private List<Argument> arguments = null;
    @JsonProperty("location")
    private String location;

    @JsonProperty("arguments")
    public List<Argument> getArguments() {
        if (CollectionUtils.isEmpty(arguments)) {
            arguments = new ArrayList<>();
        }
        return arguments;
    }

    @JsonProperty("arguments")
    public void setArguments(List<Argument> arguments) {
        this.arguments = arguments;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

}
