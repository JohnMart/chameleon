package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Table;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebTable;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestWebtable extends WebBaseTest {
    ExtendedWebDriver driver = null;

    private String xpath = "//table";

    @BeforeClass(groups = { "regression", "interfaces", "webtable", "dev" })
    public void setup() {
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/webtable.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @AfterClass(groups = { "regression", "interfaces", "webtable", "dev" })
    public void close(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest("TestAlert", testResults);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" })
    public void constructorWithElement() {

        driver = testStart("TestWebtable");
        Assert.assertNotNull((new WebTable(driver, (By.xpath(xpath)))));
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void clickCell() {
        Table webtable = driver.findTable(By.xpath(xpath));
        webtable.clickCell(1, 1);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getCell() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getCell(2, 2).getText().equals("Sylvester"));

    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getCellData() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getCellData(2, 2).equals("Sylvester"));
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getColumnCount() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getColumnCount(1) == 5);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getColumnWithCellText() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getColumnWithCellText("Last Name") == 3);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getColumnWithCellTextUsingRow() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getColumnWithCellText("Agassi", 5) == 3);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getColumnWithCellTextNothingFound() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getColumnWithCellText("Blah") == 0);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowCount() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowCount() == 7);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellText() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("Schwarzenegger") == 6);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellTextSpecifiedColumn() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("Movie", 5) == 2);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellTextSpecifiedRowAndColumn() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("Male", 4, 4) == 5);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellTextNotExact() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("SpOrT", 5, 3, false) == 5);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellTextNothingFound() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("NOTHING!", 2, 2, true) == 0);
    }

    @Feature("Element Interfaces")
    @Story("Webtable")

    @Test(groups = { "regression", "interfaces", "webtable" }, dependsOnMethods = "constructorWithElement")
    public void getRowWithCellTextNoColumn() {
        Table webtable = driver.findTable(By.xpath(xpath));
        Assert.assertTrue(webtable.getRowWithCellText("Science", -1, 2, false) == 4);
    }
}
