package com.chameleon.selenium.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.chameleon.AutomationException;
import com.chameleon.BaseTest;
import com.chameleon.ChameleonGlobalProperties;
import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverManagerFactory;
import com.chameleon.selenium.DriverOptionsManager;
import com.chameleon.selenium.DriverProperties;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.ExtendedDriver;
import com.chameleon.utils.EncryptDecryptHelper;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.io.PropertiesManager;
import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.saucerest.SauceREST;

/**
 *
 * @author Justin Phlegar & Waightstill W Avery
 * @summary This class is designed to be extended by page classes and
 *          implemented by test classes. It houses test environment data and
 *          associated getters and setters, setup for both local and remote
 *          WebDrivers as well as page class methods used to sync page behavior.
 *          The need for this arose due to the parallel behavior that indicated
 *          that WebDriver instances were crossing threads and testing on the
 *          wrong os/browser configurations
 * @date April 5, 2015
 *
 */
public class WebBaseTest extends BaseTest {
    /*
     * Test Environment Fields
     */
    protected static ThreadLocal<String> applicationUnderTest = new ThreadLocal<>();
    protected static ThreadLocal<String> browserUnderTest = new ThreadLocal<>();
    protected static ThreadLocal<String> browserVersion = new ThreadLocal<>();
    protected static ThreadLocal<String> operatingSystem = new ThreadLocal<>();
    protected static ThreadLocal<String> runLocation = new ThreadLocal<>();
    protected static ThreadLocal<String> pageUrl = new ThreadLocal<>();

    /*
     * WebDriver Fields
     */
    protected static ThreadLocal<String> sessionId = new ThreadLocal<>();

    /*
     * Getters and setters
     */
    public void setApplicationUnderTest(String aut) {
        applicationUnderTest.set(aut);
    }

    public String getApplicationUnderTest() {
        return applicationUnderTest.get();
    }

    public void setPageURL(String url) {
        pageUrl.set(url);
    }

    public String getPageURL() {
        return pageUrl.get();
    }

    public void setBrowserUnderTest(String but) {
        if (but.equalsIgnoreCase("jenkinsParameter")) {
            browserUnderTest.set(System.getProperty("jenkinsBrowser").trim());
        } else {
            browserUnderTest.set(but);
        }
    }

    public String getBrowserUnderTest() {
        return browserUnderTest.get() == null ? "" : browserUnderTest.get();
    }

    public void setBrowserVersion(String bv) {
        if (bv.equalsIgnoreCase("jenkinsParameter")) {
            if (System.getProperty("jenkinsBrowserVersion") == null
                    || System.getProperty("jenkinsBrowserVersion") == "null") {
                browserVersion.set("");
            } else {
                browserVersion.set(System.getProperty("jenkinsBrowserVersion").trim());
            }
        } else {
            browserVersion.set(bv);
        }
    }

    public String getBrowserVersion() {
        return browserVersion.get();
    }

    protected void setRunLocation(String location) {
        if (location.equalsIgnoreCase("jenkinsParameter")) {
            runLocation.set(System.getProperty("jenkinsRunLocation".trim()));
        } else {
            runLocation.set(location);
        }
    }

    public String getRunLocation() {
        return runLocation.get() == null ? "" : runLocation.get();
    }

    public void setOperatingSystem(String os) {
        if (os.equalsIgnoreCase("jenkinsParameter")) {
            operatingSystem.set(System.getProperty("jenkinsOperatingSystem").trim());
        } else {
            operatingSystem.set(os);
        }
    }

    public String getOperatingSystem() {
        return operatingSystem.get() == null ? "" : operatingSystem.get();
    }

    public String getRemoteURL() {
        if (getRunLocation().equalsIgnoreCase("sauce")) {
            final String user = DriverProperties.getSaucelabsUsername();
            final String key = DriverProperties.getSaucelabsKey();

            if (StringUtils.isEmpty(user) || StringUtils.isEmpty(key)) {
                throw new WebException("Missing required properties [ chameleon.selenium.hub.saucelabs.username ] or [ chameleon.selenium.hub.saucelabs.key ]. These need to be added in global config properties");
            }

            final String decryptedUser = EncryptDecryptHelper.decrypt(user);
            final String decryptedKey = EncryptDecryptHelper.decrypt(key);

            final SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(decryptedUser, decryptedKey);

            final String sauceLabsURL = "http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub";
            return sauceLabsURL;
        } else if (getRunLocation().equalsIgnoreCase("grid")) {
            final String url = DriverProperties.getWebHubUrl();
            if (StringUtils.isEmpty(url)) {
                throw new WebException("Missing required properties [ chameleon.selenium.hub.web.url ] . These need to be added in global config properties");
            }
            return url;
        } else {
            return "";
        }
    }

    // ************************************
    // ************************************
    // ************************************
    // WEBDRIVER SETUP
    // ************************************
    // ************************************
    // ************************************

    /**
     * Doubling up to cover different threading between before test and before method
     *
     * @param browserUnderTest
     * @param browserVersion
     * @param operatingSystem
     */
    @Parameters({ "browserUnderTest", "browserVersion", "operatingSystem", "runLocation", "application" })
    @BeforeSuite(alwaysRun = true)
    public void beforeWebTest(@Optional String browserUnderTest, @Optional String browserVersion, @Optional String operatingSystem, @Optional String runLocation, @Optional String application) {
        setBrowserUnderTest(StringUtils.isNotEmpty(browserUnderTest) ? browserUnderTest : DriverProperties.getDefaultBrowser());
        setBrowserVersion(StringUtils.isNotEmpty(browserVersion) ? browserVersion : "");
        setOperatingSystem(StringUtils.isNotEmpty(operatingSystem) ? operatingSystem : "");
        setApplicationUnderTest(StringUtils.isNotEmpty(application) ? application : "");
        setRunLocation(StringUtils.isNotEmpty(runLocation) ? runLocation : "local");
    }

    /**
     * Doubling up to cover different threading between before test and before method
     *
     * @param browserUnderTest
     * @param browserVersion
     * @param operatingSystem
     */
    @Parameters({ "browserUnderTest", "browserVersion", "operatingSystem", "runLocation", "application" })
    @BeforeMethod(alwaysRun = true)
    public void beforeWebMethod(@Optional String browserUnderTest, @Optional String browserVersion, @Optional String operatingSystem, @Optional String runLocation, @Optional String application) {
        setBrowserUnderTest(StringUtils.isNotEmpty(browserUnderTest) ? browserUnderTest : DriverProperties.getDefaultBrowser());
        setBrowserVersion(StringUtils.isNotEmpty(browserVersion) ? browserVersion : "");
        setOperatingSystem(StringUtils.isNotEmpty(operatingSystem) ? operatingSystem : "");
        setRunLocation(StringUtils.isNotEmpty(runLocation) ? runLocation : "local");
        setApplicationUnderTest(StringUtils.isNotEmpty(application) ? application : "");
        setEnvironment(StringUtils.isNotEmpty(getEnvironment()) ? getEnvironment() : null);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
        endTest(getTestName(), testResults);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        endTest(getTestName(), testResults);
    }

    /**
     * Initializes the webdriver, sets up the run location, driver type,
     * launches the application. Gives the option of using the EnvironmentURL properties
     * file to launch the URL of the application, or you can set the page URL during setup by calling
     * setPageURL("http://urlforthepage.com"). Unless you are wanting the test to start from a specific
     * page in the application under test, you will not set that field & will instead just use the base
     * URL from the properties file
     *
     * @version 12/16/2014
     * @author Jessica Marshall
     */
    public ExtendedWebDriver testStart(String testName) {
        setTestName(testName);
        driverSetup();
        // launch the application under test normally
        if (getPageURL() == null || getPageURL().isEmpty()) {
            if (StringUtils.isEmpty(getEnvironment())) {
                DriverManager.getDriver().launchApplication(getApplicationUnderTest());
            } else {
                DriverManager.getDriver().launchApplication(getApplicationUnderTest(), getEnvironment());
            }
        } // Otherwise if you have a specific page you want the test to start from
        else {
            DriverManager.getDriver().get(pageUrl.get());
        }
        return DriverManager.getWebDriver();
    }

    /**
     * Ends the test and grabs the test result (pass/fail) in case need to use that
     * for additional reporting - such as to a sauce labs run. Allows for different
     * ways of quiting the browser depending on r
     * Use ITestResult from @AfterMethod to determine run status before closing
     * test if reporting to sauce labs
     */
    protected void endTest(String testName, ITestResult testResults) {

        ExtendedWebDriver driver = null;
        try {
            driver = DriverManager.getWebDriver();
        } catch (AutomationException e) {

        }
        if (driver != null && driver.getWebDriver() != null && !driver.getWebDriver().toString().contains("null") &&
                DriverManager.getDriver().getWebDriver().getWindowHandles().size() > 0) {
            if (getRunLocation().equalsIgnoreCase("sauce")) {
                // Sauce labs specific to end test
                if (getRunLocation().equalsIgnoreCase("sauce")) {
                    reportToSauceLabs(testResults.getStatus());
                }
            }
            // quit driver
            DriverManager.quitDriver();
            DriverManager.stopService();
        }
    }

    /**
     * Ends the test and grabs the test result (pass/fail) in case need to use that
     * for additional reporting - such as to a sauce labs run. Allows for different
     * ways of quiting the browser depending on run status
     * Use ITestContext from @AfterTest or @AfterSuite to determine run status
     * before closing test if reporting to sauce labs
     */
    protected void endTest(String testName, ITestContext testResults) {
        ExtendedDriver driver = null;
        try {
            driver = DriverManager.getDriver();
        } catch (AutomationException e) {

        }
        if (driver != null && driver.getWebDriver() != null && !driver.getWebDriver().toString().contains("null") &&
                DriverManager.getDriver().getWebDriver().getWindowHandles().size() > 0) {
            if (getRunLocation().equalsIgnoreCase("sauce")) {
                if (testResults.getFailedTests().size() == 0) {
                    reportToSauceLabs(ITestResult.SUCCESS);
                } else {
                    reportToSauceLabs(ITestResult.FAILURE);
                }
            }
            // quit driver
            DriverManager.quitDriver();
            DriverManager.stopService();
        }

    }

    /**
     * Ends the test for a sauce labs run by passing in the test results (pass/fail)
     * and quits
     *
     * @param result
     */
    private void reportToSauceLabs(final int result) {
        final Map<String, Object> updates = new HashMap<>();
        updates.put("name", getTestName());

        if (result == ITestResult.FAILURE) {
            updates.put("passed", false);
        } else {
            updates.put("passed", true);
        }

        final String user = DriverProperties.getSaucelabsUsername();
        final String key = DriverProperties.getSaucelabsKey();

        if (StringUtils.isEmpty(user) || StringUtils.isEmpty(key)) {
            throw new WebException("Missing required properties [ chameleon.selenium.hub.saucelabs.username ] or [ chameleon.selenium.hub.saucelabs.key ]. These need to be added in global config properties");
        }

        final String decryptedUser = EncryptDecryptHelper.decrypt(user);
        final String decryptedKey = EncryptDecryptHelper.decrypt(key);

        final SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(decryptedUser, decryptedKey);
        SauceREST client = new SauceREST(authentication.getUsername(), authentication.getAccessKey());
        client.updateJobInfo(DriverManager.getDriver().getSessionId(), updates);
    }

    /**
     * Sets up the driver type, location, browser under test, os
     *
     * @param None
     * @version 12/16/2014
     * @author Justin Phlegar
     * @return Nothing
     * @throws IOException
     * @throws InterruptedException
     */
    private void driverSetup() {
        // local execution
        if (getRunLocation().equalsIgnoreCase("local")) {
            localDriverSetup();

            // Code for running on remote execution such as a selenium grid or saucelabs
        } else if (getRunLocation().equalsIgnoreCase("grid") || getRunLocation().equalsIgnoreCase("sauce")) {
            remoteDriverSetup();
        } else {
            throw new AutomationException(
                    "Parameter for run [Location] was not set to 'Local', 'Grid', 'Sauce', 'Mobile'");
        }

        // Microsoft Edge Browser
        if (!getRunLocation().equalsIgnoreCase("mobile")) {
            DriverManager.getDriver().manage().deleteAllCookies();
            DriverManager.getDriver().manage().window().maximize();
        }

        Map<String, String> props = null;
        try {
            if (StringUtils.isNotEmpty(getEnvironment())) {
                props = PropertiesManager.properties(String.format("config/%s/%s-%s.properties", getApplicationUnderTest(), getApplicationUnderTest(), getEnvironment()));
            } else {
                props = PropertiesManager.properties(String.format("config/%s/%s.properties", getApplicationUnderTest(), getApplicationUnderTest()));
            }
        } catch (AutomationException e) {

        }
        if (props != null) {
            setApplicationConfigs(props);
        }

        DriverManager.getDriver().setElementTimeout(DriverProperties.getElementTimeout());
        DriverManager.getDriver().setPageTimeout(DriverProperties.getPageTimeout());
    }

    /**
     * Creates a local web driver instance based on browser, browser version (required only for firefox).
     * It uses driver servers for each browser that are stored within the project.
     * For firefox versions greater than 46, you will need to use the marionette/gecko driver.
     *
     * @author jessica.marshall
     * @date 9/13/2016
     */
    private void localDriverSetup() {
        if (DriverType.HTML.equals(DriverType.fromString(getBrowserUnderTest()))) {
            DriverOptionsManager options = new DriverOptionsManager();
            options.getFirefoxOptions().setHeadless(true);
            setBrowserUnderTest("firefox");
            DriverManagerFactory.getManager(DriverType.fromString(getBrowserUnderTest()), options).initalizeDriver();
        } else {
            DriverManagerFactory.getManager(DriverType.fromString(getBrowserUnderTest())).initalizeDriver();
        }
    }

    /**
     * Creates the remote webdriver instance based on browser, browser version
     * OS, and the remote grid URL
     *
     * @author jessica.marshall
     * @date 9/13/2016
     */
    private void remoteDriverSetup() {
        DriverOptionsManager options = new DriverOptionsManager();
        DriverType type = DriverType.fromString(getBrowserUnderTest());

        if (!getBrowserVersion().isEmpty()) {
            // Setting Browser version if desired
            options.setBrowserVersion(type, getBrowserVersion());
        }

        // Setting default Broswer options
        switch (DriverType.fromString(getBrowserUnderTest())) {
            case SAFARI:
                // options.getSafariOptions().useCleanSession(true);
                // options.getSafariOptions().setCapability(SafariOptions.CAPABILITY, options.getSafariOptions());
                break;
            case INTERNETEXPLORER:
                options.getInternetExplorerOptions().ignoreZoomSettings();
                break;
            default:
                break;
        }

        // Operating System
        options.setPlatform(type, getOperatingSystem());
        options.setCapability(type, "name", getTestName());
        // Create the remote web driver
        URL url = null;
        try {
            url = new URL(getRemoteURL());

        } catch (MalformedURLException e) {
            throw new AutomationException("Failed to create Remote WebDriver", e);
        }
        DriverManagerFactory.getManager(type, options).initalizeDriver(url);
        // allows for local files to be uploaded via remote webdriver on grid machines
        DriverManager.getDriver().setFileDetector();
    }

    private void setApplicationConfigs(Map<String, String> props) {
        ChameleonGlobalProperties.initialize(props);
        DriverProperties.initialize(props);
        if (TestReporter.getDebugLevel() == 0) {
            TestReporter.setDebugLevel(ChameleonGlobalProperties.getDefaultLoggingLevel());
        }

        if (!TestReporter.getPrintToConsole()) {
            TestReporter.setPrintToConsole(ChameleonGlobalProperties.getDefaultPrintToConsole());
        }
    }
}
