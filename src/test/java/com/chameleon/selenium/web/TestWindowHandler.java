package com.chameleon.selenium.web;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.WindowHandler;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestWindowHandler extends WebBaseTest {
    ExtendedWebDriver driver = null;

    @BeforeClass(groups = { "regression", "utils", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("http://google.com");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Utilities")
    @Story("WindowHandler")
    
    @Test(groups = { "regression", "smoke" })
    public void waitUntilNumberOfWindowsAre() {
        driver = testStart("TestWindowHandler");
        driver.executeJavaScript("window.open('https://gitlab.com/orasi-backup/Chameleon', 'BLAH', 'height=800,width=800');");
        Assert.assertTrue(WindowHandler.waitUntilNumberOfWindowsAre(driver, 2));
    }

    @Feature("Utilities")
    @Story("WindowHandler")
    
    @Test(groups = { "regression", "smoke" }, dependsOnMethods = "waitUntilNumberOfWindowsAre")
    public void waitUntilWindowExistsWithTitle() {
        Assert.assertTrue(WindowHandler.waitUntilWindowExistsWithTitle(driver, "Sign in � GitLab"));
    }

    @Feature("Utilities")
    @Story("WindowHandler")
    
    @Test(groups = { "regression", "smoke" }, dependsOnMethods = "waitUntilNumberOfWindowsAre")
    public void waitUntilWindowExistsTitleContains() {
        Assert.assertTrue(WindowHandler.waitUntilWindowExistsTitleContains(driver, "GitLab"));
    }

    @Feature("Utilities")
    @Story("WindowHandler")
    
    @Test(groups = { "regression", "smoke" }, dependsOnMethods = "waitUntilNumberOfWindowsAre")
    public void waitUntilWindowExistsTitleMatches() {
        Assert.assertTrue(WindowHandler.waitUntilWindowExistsTitleMatches(driver, "(?i:.*Lab)"));
    }

}
