package com.chameleon.utils.exception;

import com.chameleon.AutomationException;

public class PDFValidationException extends AutomationException {
    private static final long serialVersionUID = 3407361723082329697L;

    public PDFValidationException(String message) {
        super(message);
    }

    public PDFValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
